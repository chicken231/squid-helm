# Squid Helm Chart

- [Squid Helm Chart](#squid-helm-chart)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Install / Upgrade / Delete](#install--upgrade--delete)
  - [Contributing](#contributing)
  - [Versioning](#versioning)
  - [License](#license)
  - [Acknowledgments](#acknowledgments)

A Helm Chart to run a `Deployment` of Squid through a LoadBalancer (By default).

## Getting Started

### Prerequisites

```bash
git clone https://gitlab.com/chicken231/squid-helm
```

### Install / Upgrade / Delete

```bash
# install
helm install --name squid ./squid-helm
# upgrade
helm upgrade squid ./squid-helm
# delete
helm del --purge squid
```

## Contributing

See [CONTRIBUTING.md](https://gitlab.com/chicken231/squid-helm/CONTRIBUTING.md).

## Versioning

[SemVer](http://semver.org/) is used for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## License

This project is licensed under the **MIT** License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

+ [*sameersbn's* Squid Docker Image](https://github.com/sameersbn/docker-squid)
* [A helpful guide that kept me on track](http://www.pc-freak.net/blog/build-install-docker-squid-open-proxy-image-kubernetes-cluster/)
